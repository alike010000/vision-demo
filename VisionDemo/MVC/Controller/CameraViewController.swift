//
//  CameraViewController.swift
//  VisionDemo
//
//  Created by Alik Elimkhajiyev on 6/6/18.
//  Copyright © 2018 alike. All rights reserved.
//

// -- FRAMEWORKS
import UIKit
import AVFoundation
import CoreML
import Vision

enum SwitchState {
    case off
    case on
}

class CameraViewController: UIViewController {
    
    // -- VARIABLES
    // CAMERA
    var captureSession: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var photoData: Data?
    
    // SWITCHES
    var flashButtonState: SwitchState = .off
    var voiceButtonState: SwitchState = .off
    
    // SPEECH
    var speechSynthesizer = AVSpeechSynthesizer()
    
    // -- OUTLETS
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var voiceButton: UIButton!
    @IBOutlet weak var spinnerIndicator: UIActivityIndicatorView!
    
    // HIDE STATUS BAR
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // -- LIFECYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // SETUP TAP GESTURE
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapCameraView))
        tap.numberOfTapsRequired = 1
        
        // CREATE CAMERA
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)

        do {
            let input = try AVCaptureDeviceInput(device: backCamera!)
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
            }
            cameraOutput = AVCapturePhotoOutput()

            if captureSession.canAddOutput(cameraOutput) {
                captureSession.addOutput(cameraOutput)

                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait

                cameraView.layer.addSublayer(previewLayer!)
                cameraView.addGestureRecognizer(tap)
                captureSession.startRunning()
            }
        } catch {
            debugPrint(error)
        }
    }
    
    // SHOW CAMERA PREVIEW ON A SCREEN
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = cameraView.bounds
        speechSynthesizer.delegate = self
        spinnerIndicator.isHidden = true
    }
    
    // CAPTURING PICTURES
    @objc func didTapCameraView() {
        let settings = AVCapturePhotoSettings()
        settings.previewPhotoFormat = settings.embeddedThumbnailPhotoFormat
        
        // CHECKING FLASH
        if flashButtonState == .off {
            settings.flashMode = .off
        } else {
            settings.flashMode = .on
        }
        
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
    
    // GETTING RESULTS FROM COREML
    func resultsMethod(request: VNRequest, error: Error?) {
        guard let results = request.results as? [VNClassificationObservation] else { return }
        for classification in results {
            print(classification.identifier)
            // CHECKING IF CONFIDENCE IS MORE THAN 50 PERCENT
            if classification.confidence < 0.5 {
                
                // UPDATING UI AND PRONOUNCING WITH RESULT
                let unknownObjectMessage = "I see something. But I'm not sure what it is"
                self.itemNameLabel.text = "I see something..."
                self.confidenceLabel.text = "But I'm not sure what it is"
                synthesizeSpeech(fromString: unknownObjectMessage)
                break
            } else {
                
                // UPDATING UI AND PRONOUNCING WITHOUT RESULT
                self.itemNameLabel.text = "I think it's \(classification.identifier)"
                self.confidenceLabel.text = "And I'm \(Int(classification.confidence * 100))% sure about it"
                let knownObjectMessage = "I think it's \(classification.identifier) And I'm \(Int(classification.confidence * 100))% sure about it"
                synthesizeSpeech(fromString: knownObjectMessage)
                break
            }
        }
    }
    
    // PRONOUNCING PHRASE
    func synthesizeSpeech(fromString string: String) {
        if voiceButtonState == .on {
            self.cameraView.isUserInteractionEnabled = false
            self.spinnerIndicator.isHidden = false
            self.spinnerIndicator.startAnimating()
            
            let speechUtterance = AVSpeechUtterance(string: string)
            speechSynthesizer.speak(speechUtterance)
        }
    }
    
    // -- ACTIONS
    // SWITCHING FLASH
    @IBAction func didTapFlashButton(_ sender: Any) {
        switch flashButtonState {
        case .off:
            flashButton.setTitle("Flash on", for: .normal)
            flashButtonState = .on
            break
        case .on:
            flashButton.setTitle("Flash off", for: .normal)
            flashButtonState = .off
            break
        }
    }
    
    // SWITCHING SPEECH
    @IBAction func didTapVoiceButton(_ sender: Any) {
        switch voiceButtonState {
        case .off:
            voiceButton.setTitle("Voice on", for: .normal)
            voiceButtonState = .on
            break
        case .on:
            voiceButton.setTitle("Voice off", for: .normal)
            voiceButtonState = .off
            break
        }
    }
    
}

// CONFORMING TO PHOTO DELEGATE
extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            debugPrint(error)
        } else {
            photoData = photo.fileDataRepresentation()
            
            do {
                // PASSING PHOTO TO COREML
                let model = try VNCoreMLModel(for: SqueezeNet().model)
                let request = VNCoreMLRequest(model: model, completionHandler: resultsMethod)
                let handler = VNImageRequestHandler(data: photoData!)
                try handler.perform([request])
            } catch {
                debugPrint(error)
            }
            
            // CREATING THUMBNAIL
            let image = UIImage(data: photoData!)
            self.imageView.image = image
        }
    }
}

// CONFORMING TO SPEECH DELEGATE
extension CameraViewController: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        self.cameraView.isUserInteractionEnabled = true
        self.spinnerIndicator.stopAnimating()
        self.spinnerIndicator.isHidden = true
    }
}
