//
//  RoundedView.swift
//  VisionDemo
//
//  Created by Alik Elimkhajiyev on 6/6/18.
//  Copyright © 2018 alike. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    override func awakeFromNib() {
        configureUI()
    }
    
    func configureUI() {
        // CHECK IF THE SHAPE IS PLACED VERTICAL OR HORIZONTAL
        if self.frame.width <= self.frame.height {
            self.layer.cornerRadius = self.frame.width / 12
        } else {
            self.layer.cornerRadius = self.frame.height / 12
        }
    }
}
